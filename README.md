MERN STACK

create the backend and frontend directory
initiate the package.json in backend. (npm init -y)
install the express and nodemon packages

add the start and dev script reference to package.json, and create module as type in the package.json file
create the same index.js file in the backend directory

create the config.js file and export the variable "PORT"
create the call back function for the app.listen to listen on the port imported from the config.js
run the dev via 'npm run dev' will run the nodemon and we can check it on browser 'localhost:PORT'
create the get method

create the mongoose connection function
create the book model and save it to mongoose.
create the CRUD methods in routes folder
use the cors policy in index.js


frontend

use vite
npm install vite@latest and select the react, javascript. Follow the commands.
install the react-router-dom and change the main.jsx file (browser-routes added as route)
create pages for different CURD operation and import them in app.jsx file. In main.jsx add the routes in app object
install axios and react-icons
create spinner.jsx (why?)
create different pages for front end
install notistack, react react-router-dom
