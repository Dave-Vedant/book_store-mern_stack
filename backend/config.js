import {dbPassword} from './secret.js'

export const PORT = 5555;
export const mongoDBURL = `mongodb+srv://root:${encodeURIComponent(dbPassword)}@bookstore.kafcett.mongodb.net/?retryWrites=true&w=majority&appName=bookstore`;