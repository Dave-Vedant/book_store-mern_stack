import express from 'express';
import { PORT, mongoDBURL } from './config.js';
import mongoose from 'mongoose';
import bookRoute from './routes/booksRoute.js';
import cors from 'cors';


const app = express();


//parse json response
app.use(express.json());

// allows everything
app.use(cors()); 

/*
// allow custom origin
app.use(
    cors({
        origin: 'http://localhost:5555',
        method: ['GET', 'POST', 'PUT', 'DELETE'],
        allowedHeaders: ['content-Type'],
    })
);
*/

// get all the books from the database
app.get('/',( request, response) => {
    console.log(request);
    return response.status(234).send('Welcome to MERN stack');
});

app.use('/books', bookRoute);

mongoose
.connect(mongoDBURL)
.then(() => {
    console.log('App connected to database');
    app.listen(PORT, () => {
        console.log(`App is listening to port: ${PORT}`);
    });
    })
    .catch((error) => {
    console.log(error);
    });

