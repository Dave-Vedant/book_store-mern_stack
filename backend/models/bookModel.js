import mongoose from "mongoose";

//create book schema
const bookSchema = mongoose.Schema(
    {
        title: {
            type: String,
            required: true,    
        },
        author: {
            type: String,
            required: true,
        },
        publishYear: {
            type: Number,
            required: true,
        },
    },
    {
        timestamps:true,
    }
);


// export to router and index.js
export const Book = mongoose.model('book-model', bookSchema);
